﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tweetinvi;

namespace TweetSmartie
{
    public class LCDSmartie
    {
        /// <summary>
        /// This function returns the latest tweet from your feed
        /// </summary>
        /// <param name="param1">User token + user secret, combined into one param</param>
        /// <param name="param2">Consumer key + consumer secret, combined into one param</param>
        /// <returns></returns>
        public string function1(string param1, string param2)
        {
            //Never hardcode publish these tokens and keys! 
            //This will allow anyone with access to the code to publish tweets to your account!
            string AccessToken = param1.Substring(0, 50);
            string AccessSecret = param1.Substring(50, 45);
            string ConsumerKey = param2.Substring(0, 25);
            string ConsumerSecret = param2.Substring(25, 50);

            //Login
            try
            {
                TwitterCredentials.SetCredentials(AccessToken, AccessSecret, ConsumerKey, ConsumerSecret);
                TwitterCredentials.ApplicationCredentials = TwitterCredentials.CreateCredentials(AccessToken, AccessSecret, ConsumerKey, ConsumerSecret);

                var credentials = TwitterCredentials.CreateCredentials(AccessToken, AccessSecret, ConsumerKey, ConsumerSecret);
                TwitterCredentials.ExecuteOperationWithCredentials(credentials, () =>
                {
                    Tweet.PublishTweet("Tweeted from my LCDSmartie library.");
                });

                return "Tweet published!";
            }
            catch (Exception ex)
            {
                return ex.InnerException.Message;
            }
        }
    }
}
